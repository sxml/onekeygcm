﻿#!/usr/bin/env perl

use Mojo::Webqq;
use Digest::MD5 qw(md5 md5_hex md5_base64);

my $client = Mojo::Webqq->new(
    log_encoding => 'utf8'
);

$client->load('UploadQRcode');

$client->load('ShowMsg');

$client->load(
    'GCM',
    data => {
        api_url => 'https://gcm-http.googleapis.com/gcm/send',
        api_key => 'AIzaSyB18io0hduB_3uHxKD3XaebPCecug27ht8',
        registration_ids => [ 'your_registration_id' ]
    }
);

$client->load(
    'Openqq',
    data => {
        listen => [
            {
                host => '0.0.0.0',
                port => 4433,
                tls => 1,
                tls_cert => '/your/ssl/certificate',
                tls_key => '/your/ssl/private/key',
                tls_version => [ 'TLSv1', 'TLSv1_1', 'TLSv1_2' ],
                tls_ciphers => 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4'
            }
        ],
        auth => sub {
            my ( $param, $controller ) = @_;
            my $secret = 'your_salt';
            my $text = '';
            foreach $key ( sort keys %$param ) {
                if ( $key ne 'sign' ) {
                    $value = $param->{$key};
                    $text .= $value;
                }
            }
            if ( $param->{sign} eq md5_hex( $text . $secret ) ) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
);

$client->run();
