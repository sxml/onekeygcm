#!/bin/bash

# -------------------------------------------------------- #
# OneKeyGcm
#
# Description:
#     Install Mojo-Webqq And Mojo-Weixin With Push Plugin.
#
# Author:
#     Aurore <code@support.aurore.me>
#
# License:
#     GNU General Public License v3.0
#
# Update:
#     2017.03.26
# -------------------------------------------------------- #

export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

clear

if [ $(id -u) != '0' ]; then
    echo 'You Must Run This Script As ROOT!'
    exit 1
fi

SetConfig () {
    echo 'Please Select The Installation Module:'
    echo -e "1.Webqq\n2.Weixin\n3.All"
    while [[ ${InstallModule} != 1 && ${InstallModule} != 2 && ${InstallModule} != 3 ]]; do
        read -p 'Select: ' InstallModule
        case ${InstallModule} in
            1) InstallModule=Webqq
               break
            ;;
            2) InstallModule=Weixin
               break
            ;;
            3) InstallModule=All
               break
            ;;
            *) echo 'You Must Select A Number Between 1 To 3!'
            ;;
        esac
    done

    echo 'Please Select The Push Type:'
    echo -e "1.GCM\n2.MiPush\n3.HuaweiPush\n4.FlymePush"
    while [[ ${PushType} != 1 && ${PushType} != 2 && ${PushType} != 3 && ${PushType} != 4 ]]; do
        read -p "Select: " PushType
	    case ${PushType} in
            1) PushType=GCM
               break
            ;;
            2) PushType=MiPush
               break
            ;;
            3) PushType=HwPush
               break
            ;;
            4) PushType=FmPush
               break
            ;;
            *) echo 'You Must Select A Number Between 1 To 3!'
            ;;
	    esac
    done

    while [ -z ${RegistrationID} ]; do
        read -p 'Please Input The Registration ID: ' RegistrationID
    done

    read -p "Please Input Config Dir (Default: $(pwd)) : " ConfigDir
    if [ -z ${ConfigDir} ]; then
        ConfigDir=$(pwd)
    fi

    read -p 'Please Input Listen Address (Default: 0.0.0.0) : ' MojoHost
    if [ -z ${MojoHost} ]; then
        MojoHost=0.0.0.0
    fi

    if [ ${InstallModule} = 'Webqq' ]; then
        read -p 'Please Input Webqq Listen Port (Default: 5000) : ' OpenqqPort
        if [ -z ${OpenqqPort} ]; then
            OpenqqPort=5000
        fi
    elif [ ${InstallModule} = 'Weixin' ]; then
        read -p 'Please Input Weixin Listen Port (Default: 3000) : ' OpenwxPort
        if [ -z ${OpenwxPort} ]; then
            OpenwxPort=3000
        fi
    else
        read -p 'Please Input Webqq Listen Port (Default: 5000) : ' OpenqqPort
        if [ -z ${OpenqqPort} ]; then
            OpenqqPort=5000
        fi
        read -p 'Please Input Weixin Listen Port (Default: 3000) : ' OpenwxPort
        if [ -z ${OpenwxPort} ]; then
            OpenwxPort=3000
        fi
    fi
}

YumInstall () {
    yum -y install \
        perl \
        perl-CPAN \
        openssl \
        openssl-devel \
        gcc \
        make \
        curl \
        ca-certificates \
        unzip
}

AptGetInstall () {
    apt-get update
    apt-get -y install --no-install-recommends \
        perl \
        openssl \
        libssl-dev \
        gcc \
        make \
        curl \
        ca-certificates \
        unzip
}

if [[ -f /etc/redhat-release || -f /etc/debian_version ]]; then
    SetConfig
    if [ -f /etc/redhat-release ]; then
        YumInstall
        curl -L https://cpanmin.us | perl - App::cpanminus
    elif [ -f /etc/debian_version ]; then
        AptGetInstall
        curl -L https://cpanmin.us | perl - App::cpanminus
    fi
else
    echo 'This Script Can Not Support Your System!'
    exit 1
fi

InstallWebqq () {
    curl -o Mojo-Webqq.zip -L https://github.com/sjdy521/Mojo-Webqq/archive/master.zip
    unzip Mojo-Webqq.zip
    pushd Mojo-Webqq-master
    cpanm -n .
    popd
    rm -rf Mojo-Webqq.zip Mojo-Webqq-master
}

SetupWebqq () {
    curl -o ${ConfigDir}/Webqq.pl -L https://gitlab.com/sxml/onekeygcm/raw/master/webqq_default_config.pl
    if [ ${PushType} != 'GCM' ]; then
        sed -i "s/GCM/${PushType}/" ${ConfigDir}/Webqq.pl
        sed -i '/\s*api_url.*/d' ${ConfigDir}/Webqq.pl
        sed -i '/\s*api_key.*/d' ${ConfigDir}/Webqq.pl
    fi
    sed -i "s/xxx/${RegistrationID}/" ${ConfigDir}/Webqq.pl
    sed -i "s/0.0.0.0/${MojoHost}/" ${ConfigDir}/Webqq.pl
    sed -i "s/5000/${OpenqqPort}/" ${ConfigDir}/Webqq.pl
}

InstallWeixin () {
    curl -o Mojo-Weixin.zip -L https://github.com/sjdy521/Mojo-Weixin/archive/master.zip
    unzip Mojo-Weixin.zip
    pushd Mojo-Weixin-master
    cpanm -n .
    popd
    rm -rf Mojo-Weixin.zip Mojo-Weixin-master
}

SetupWeixin () {
    curl -o ${ConfigDir}/Weixin.pl -L https://gitlab.com/sxml/onekeygcm/raw/master/weixin_default_config.pl
    if [ ${PushType} != 'GCM' ]; then
        sed -i "s/GCM/${PushType}/" ${ConfigDir}/Weixin.pl
        sed -i '/\s*api_url.*/d' ${ConfigDir}/Weixin.pl
        sed -i '/\s*api_key.*/d' ${ConfigDir}/Weixin.pl
    fi
    sed -i "s/xxx/${RegistrationID}/" ${ConfigDir}/Weixin.pl
    sed -i "s/0.0.0.0/${MojoHost}/" ${ConfigDir}/Weixin.pl
    sed -i "s/3000/${OpenwxPort}/" ${ConfigDir}/Weixin.pl
}

if [ ${InstallModule} = 'Webqq' ]; then
    InstallWebqq
    SetupWebqq
elif [ ${InstallModule} = 'Weixin' ]; then
    InstallWeixin
    SetupWeixin
else
    InstallWebqq
    SetupWebqq
    InstallWeixin
    SetupWeixin
fi

echo 'Install Successful!'
